<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
define('WP_MEMORY_LIMIT', '512M');
define('WP_MAX_MEMORY_LIMIT', '512M');

 if (strpos($_SERVER['REQUEST_URI'], 'wp-admin')) define ('WPLANG', 'ru_RU'); else define ('WPLANG', 'ru_RU_lite');
 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'povestka_main');

/** MySQL database username */
define('DB_USER', 'povestka_main');

/** MySQL database password */
define('DB_PASSWORD', 'B9e0V4i2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OR@b&f/j+9P@Yj7ARm> MsTAH]MQXG0aK.r]tZaf#[nw+HT<,`t$UR@+wEwJz*X:');
define('SECURE_AUTH_KEY',  '}#cg@,L+,.g^H=9WH&ajZs#Zi$P5RA(1gh|n9^c$63~P;Y;JJGAN,/uj*WNC/z^|');
define('LOGGED_IN_KEY',    '8~rqN8+iS]hqG2)cz_R<fk:ci u06mj24>L[^%K#||b|YLd~-JInwwFhOV-&QhB2');
define('NONCE_KEY',        'MxX!2PTI@9M6BxztQ %Yk8cCZl-c85zi;|RD|BfY2&+|TvA>Jeg-z%!CKcGqOE!4');
define('AUTH_SALT',        ']>%vq#;4)+,w;r[zjady&)Bj`2n9~4l#a)77S?FJ06j(>dL>bM|3<k73m :D5=0O');
define('SECURE_AUTH_SALT', '[7B6duqtYdf:!J^54+r7Wp|%?35?>/9Oq ,FCP%mxp#@3d-QLg~0VRt&3o=Y6$Zp');
define('LOGGED_IN_SALT',   'WcL~T{KvG;YoD5<HJRB9Y&yTav0S$T5E!H6xIEJ|6PdV=[,e^5|E_#rP~/* BMo_');
define('NONCE_SALT',       '2]@Ey*Lkvh+3OpdF$N!-uXwm+~dwbTR[Bhq(j7H:<W0?={t#|e#&|wF+*{qZ?9-O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'povestka_wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
add_filter('xmlrpc_enabled','__return_false');
