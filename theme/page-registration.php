<?php
/* 
Template Name: Register 
*/

	global $wpdb;
	
	if (is_user_logged_in()) {
		header( 'Location:https://povestka.by/user/' );
	} else{

		$errors = array();

		if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			$display_name          = $_POST['display_name'];
			$email                 = $_POST['email'];
			$password              = $_POST['password'];
			$password_confirmation = $_POST['password_confirmation'];
			$birthday              = $_POST['birthday'];
			$term                  = $_POST['term'];
			$rules                 = $_POST['rules'];
			
			if( empty($display_name)){
				$errors['display_name'] = "Пожалуйста, представьтесь";
			}
			
			// Check email address is present and valid
//			$email = $wpdb->escape($_REQUEST['email']);
			if( !is_email( $email ) ) {
				$errors['email'] = "Пожалуйста, введите корректный email";
			}elseif( email_exists( $email ) ) {
				$errors['email'] = "Этот email уже используется. <a href='".wp_lostpassword_url()."'>Забыли пароль?</a>";
			}
	
			// Check password is valid
			if( 0 === preg_match("/.{8,}/", $password)) {
				$errors['password'] = "Пароль должен быть не менее 8 символов";
			}

			// Check password confirmation_matches
			if( 0 !== strcmp( $password, $password_confirmation )) {
				$errors['password_confirmation'] = "Пароли не совпадают";
			}

			if ( time() < strtotime('+16 years', strtotime($birthday))) {
				$errors['birthday'] = "Вам нет 16 лет.";
			}

			// Check terms of service is agreed to
			if( $term != "Да" OR $rules != "Да") {
				$errors['agreement'] = "Вам необходимо принять все соглашения";
			}
			
			if(0 === count($errors)) {
				$new_user_id = wp_create_user( $email, $password, $email );
				if ( is_wp_error( $new_user_id ) ) {
					echo $new_user_id->get_error_message();
				}
				wp_update_user( array( 'ID' => $new_user_id, 'display_name' => $display_name ) );
				update_user_meta( $new_user_id, 'birthday', $birthday );

				$creds = array();
				$creds['user_login'] = $email;
				$creds['user_password'] = $password;
				$creds['remember'] = true;

				$user = wp_signon( $creds, true );
				header( 'Location:' . get_bloginfo('url') . '/user/?start' );
			}
		}
	}
	get_header();

?>
<div class="container">
	<div class="row">
		<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jumbotron">
				<h2 class="test-center">Регистрация</h2> 
				
				<form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
					<fieldset>
					
						<div class="form-group<?php if(isset($errors['display_name'])) echo " has-error"; ?>">
							<label class="col-md-4 control-label" for="display_name">Представьтесь</label>
							<div class="col-md-5">
								<input id="display_name" name="display_name" placeholder="Введите Ваше имя или псевдоним" class="form-control input-md" required="" value="<?php if(isset($display_name)) echo $display_name; ?>" type="text" autocomplete="off">
								<span class="help-block">Имя или псевдоним будут отображаться возле Ваших вопросов, ответов, отзывов и комментариев. <?php if(isset($errors['display_name'])) echo $errors['display_name']; ?></span>
							</div>
						</div>

						<div class="form-group<?php if(isset($errors['email'])) echo " has-error"; ?>">
							<label class="col-md-4 control-label" for="email">Email</label>
							<div class="col-md-5">
								<input id="email" name="email" placeholder="Введите Ваш email" class="form-control input-md" required="" type="email" autocomplete="off" value="<?php if(isset($email)) echo $email; ?>">
								<span class="help-block"><?php if(isset($errors['email'])) echo $errors['email']; ?></span>
							</div>
						</div>

						<div class="form-group<?php if(isset($errors['password'])) echo " has-error"; ?>">
							<label class="col-md-4 control-label" for="password">Пароль</label>
							<div class="col-md-5">
								<input id="password" name="password" placeholder="Введите пароль" class="form-control input-md" required="" type="password" autocomplete="off" value="<?php if(isset($password)) echo $password; ?>">
								<span class="help-block">Пароль должен содержать не менее 8 символов. Это может быть любая комбинация букв, цифр и других допустимых знаков (символов ASCII).</span>
							</div>
						</div>

						<div class="form-group<?php if(isset($errors['password_confirmation'])) echo " has-error"; ?>">
							<label class="col-md-4 control-label" for="password_confirmation">Подтверждение пароля</label>
							<div class="col-md-5">
								<input id="password_confirmation" name="password_confirmation" placeholder="Повторите пароль" class="form-control input-md" required="" type="password" autocomplete="off" value="<?php if(isset($password_confirmation)) echo $password_confirmation; ?>">
								<span class="help-block"><?php if(isset($errors['password_confirmation'])) echo $errors['password_confirmation']; ?></span>
							</div>
						</div>

						<div class="form-group<?php if(isset($errors['birthday'])) echo " has-error"; ?>">
							<label class="col-md-4 control-label" for="birthday">Дата рождения</label>
							<div class="col-md-5">
								<input id="birthday" name="birthday" placeholder="Укажите дату вашего рождения" class="form-control input-md" required="" type="text" autocomplete="off" value="<?php if(isset($birthday)) echo $birthday; ?>">
								<span class="help-block">Регистрация доступна для лиц достигших 16 лет. <?php if(isset($errors['birthday'])) echo $errors['birthday']; ?></span>
							</div>
						</div>
						
						<div class="form-group <?php if(isset($errors['agreement'])) echo " has-error"; ?>">
							<label class="col-md-4 control-label">Соглашения</label>
							<div class="col-md-5">
								<input id="terms" name="term" value="Да" required="" type="checkbox" <?php checked( $term, 'Да' ); ?>> Я подтверждаю, что ознакомлен с <a href="http://povestka.by/help/privacy-policy/">Политикой конфиденциальности</a> и соглашаюсь с ней<br>
								<input id="rules" name="rules" value="Да" required="" type="checkbox" <?php checked( $rules, 'Да' ); ?>> Я подтверждаю, что ознакомлен с <a href="https://povestka.by/help/rules/">Правилами сайта</a> и соглашаюсь с ними
								<span class="help-block"><?php if(isset($errors['agreement'])) echo $errors['agreement']; ?></span>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="submit"> </label>
							<div class="col-md-4">
								<button id="submit" name="submit" class="btn btn-primary">Зарегистрироваться</button>
							</div>
						</div>
						
					</fieldset>
				</form>

			</div>
		</section>
	</div>
</div>
<?php get_footer(); ?>