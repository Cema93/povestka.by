<?php get_header(); ?>
<?php

$terms = get_terms( array(
	'taxonomy'      => 'wiki-cat',
	'orderby'       => 'name', 
	'hide_empty'    => false, 
	'count'         => false,
	'hierarchical'  => true, 
) );
$terms = wp_list_filter( $terms, array('parent'=>0) );
?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1>wiki</h1>
			<?php
				$prev_char = null;
				if( $terms && ! is_wp_error($terms) ){
					foreach( $terms as $term ){
						$char = mb_strtoupper(mb_substr($term->name,0,1, "UTF-8"), "UTF-8");
						if( $prev_char != $char ){
							if($prev_char != null) echo '</ul>';
							$prev_char = $char;
							echo "<h4>". $char ."</h4>";
							echo '<ul>';
						}
						echo "<li><a href='". get_term_link( (int) $term->term_id, 'wiki-cat' ) ."'>". $term->name ."</a></li>";
					}
					echo "</ul>";
				}
			?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
