<?php
	function wpshout_save_post_if_submitted() {
		global $wpdb;
		
		if ( !isset($_POST['_wp_http_referer']) OR htmlspecialchars($_POST['_wp_http_referer'], ENT_QUOTES | ENT_HTML5, 'UTF-8') !="/rating/" ) {
			return null;
		}

		if( !wp_verify_nonce($_POST['_wpnonce'], 'new_org_review_nonce') ) {
			return array( 'error' => 'Мы зафиксировали попытку взлома!');
		}

		$current_user = wp_get_current_user();
		if($current_user == 0){
			return array( 'error' => 'Авторизуйтесь, чтобы оставить отзыв!');
		}
		if(!user_can( $current_user->ID, 'ap_new_comment' ) ) {
			return array( 'error' => 'У Вас нет прав для оставления отзыва!');
		}

		$org=array();
		$posts = get_posts( array( 'post_type' => 'place', 'posts_per_page' => -1, 'place-cat' => 'sluzhby-pomoshhi-prizyvnikam', 'orderby' => array( 'title' => 'DESC' ) ) );
		foreach( $posts as $pst ){
			array_push($org, $pst->ID);
		}
		if( (int)$_POST['org'] == 0 ){
			return array( 'error' => 'Выберите организацию!');
		}elseif( !in_array((int)$_POST['org'], $org) ) {
			return array( 'error' => 'Мы зафиксировали попытку взлома!');
		}
		
		if( strlen($_POST['comment']) < 10 ) {
			return array( 'error' => 'Комментарий слишком короткий!');
		}
		
		if(!empty((int)$_POST['rating'])){
			switch ((int)$_POST['rating']) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					break;
				default:
					return array( 'error' => 'Мы зафиксировали попытку взлома!');
					break;
			}
		}else{
			return array( 'error' => 'Поставьте оценку!');
		}

		$result = $wpdb->insert( 'povestka_wp_company_rating', array(
			'user_id' => $current_user->ID,
			'org_id'  => (int)$_POST['org'],
			'comment' => $_POST['comment'], 
			'rating'  => (int)$_POST['rating']
		), array('%d', '%d', '%s', '%d'));
		if( $result == false){
			return array( 'error' => 'Ошибка созранения данных!');
		}elseif($result > 0){
			recount_org_reviews((int)$_POST['org']);
			$_POST = array();
			return array( 'success' => 'Отзыв сохранён!');
		}else{
			return array( 'error' => 'Что-то пошло не так!');
		}
		
	}
	$event = wpshout_save_post_if_submitted();
?>
<?php get_header(); ?>
	<div class="cover-container container">
		<main role="main" class="text-center">
			<h1>Рейтинг Центра прав призывника</h1>
			<p class="lead">В Беларуси есть не мало организаций, которые готовы помогать призывникам и военнослужащим. Некоторые делают это платно, некоторые бесплатно, но все они пишут у себя на сайте, что они лучшие и обращаться надо именно к ним. Так ли это на самом деле? Мы сделали независимый рейтинг, где каждый может оставить реальный отзыв и свою историю обращений в эти организации!</p>
			<p><a href="#RatingCarousel" class="btn btn-lg btn-secondary">Перейти к рейтигу</a></p>
		</main>
	</div>
	<div class="container">
		<div class="row">
			<div id="RatingCarousel" class="carousel slide">
				<div class="carousel-inner">
					<?php
//						$query = new WP_Query( array( 'post_type' => 'place', 'posts_per_page' => -1, 'place-cat' => 'sluzhby-pomoshhi-prizyvnikam', 'orderby' => array( 'title' => 'ASC' ) ) ); 



$query = new WP_Query( array( 
	'post_type' => 'place', 
	'posts_per_page' => -1, 
	'place-cat' => 'sluzhby-pomoshhi-prizyvnikam', 
	'orderby'  => array(
		'meta_value_num' => 'DESC',
		'meta_key' => 'rating',
		'title'      => 'ASC',
	),
) );






					?>
					<?php $i=0; ?>
					<?php while ( $query->have_posts() ) { $query->the_post(); ?>
					<div class="item<?php if($i++<1) { echo" active"; }?>">
						<div class="col-xs-12 col-sm-4">
							<div class="card card-price">
								<?php if(has_post_thumbnail( get_the_ID() )) { ?>
									<div class="card-img">
										<a href="<?php echo get_permalink( get_the_ID() ); ?>">
											<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>" class="img-responsive">
										</a>
									</div>
								<?php }else{ ?>
									<div class="card-img">
										<a href="<?php echo get_permalink( get_the_ID() ); ?>">
											<img src="https://via.placeholder.com/1200x550.png?text=<?php the_title_attribute() ?>" class="img-responsive">
										</a>
									</div>
								<?php } ?>
								<div class="card-body">
									<table class="table">
										<tr>
											<td>Рейтинг</td><td class="note">
												<div class="rating">
													<?php 
														if(get_org_rewiews_count(get_the_ID())){
															$i = 0;
															for ($j = 1; $j <= (int)round(get_field('rating', get_the_ID())); $j++ ) {
																echo '<i class="fa fa-star fa-2x" aria-hidden="true"></i>';
																$i++;
															}
															for($i; $i <5; $i++){
																echo '<i class="fa fa-star-o fa-2x" aria-hidden="true"></i>';
															}
																
														}else{
															echo "Отзывов пока нет";
														}
													?>
												</div>
											</td>
										</tr>
										<tr><td>Адрес</td><td class="note"><a href="#" class="show-button" data-show="<?php the_ID()?>-adr" data-stat="adr_<?php the_title_attribute() ?>">Показать</a><div class="<?php the_ID(); ?>-adr collapse"><?php the_field('Address', get_the_ID()); ?></div></td></tr>
										<tr><td>Контакты</td><td class="note"><a href="#" class="show-button" data-show="<?php the_ID()?>-cnt" data-stat="cnt_<?php the_title_attribute() ?>">Показать</a><div class="<?php the_ID(); ?>-cnt collapse"><?php the_field('contacts', get_the_ID()); ?></div></td></tr>
									</table>
									<div class="btn-group btn-group-justified" role="group">
										<a href="<?php echo get_permalink( get_the_ID() ); ?>#reviews" class="btn btn-primary btn-lg buy-now">Отзывы (<?php echo get_org_rewiews_count(get_the_ID()); ?>)</a>
										<a href="<?php echo get_permalink( get_the_ID() ); ?>#comments" class="btn btn-primary btn-lg buy-now">Обсуждение</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<a class="left carousel-control" href="#RatingCarousel" data-slide="prev"><i class="icon-prev" aria-hidden="true"></i></a>
				<a class="right carousel-control" href="#RatingCarousel" data-slide="next"><i class="icon-next" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<div class="container">
		<h1>Оставить отзыв</h1>
		<?php 
			if(isset($event['error'])) {
				echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Внимание!</strong> '. $event["error"] .'</div>';
			}elseif(isset($event['success'])) {
				echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'. $event["success"] .'</div>';
			}
		?>
	<?php
		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			if(user_can( $current_user->ID, 'ap_new_comment' ) ) {
				?>
				<form id="new_org_review" action="#new_org_review" method="post">
					<div class="form-group col-xs-12">
						<label for="user_name">Отображаемое имя</label>
						<input type="text" class="form-control" value="<?php echo $current_user->display_name; ?>" readonly>
						<small>Изменить можно через личный кабинет.</small>
					</div>
					<div class="form-group col-xs-12">
						<label for="org">Выберите организацию</label>
						<select class="form-control" name="org" required>
							<option value="">---</option>
							<?php $query = new WP_Query( array( 'post_type' => 'place', 'posts_per_page' => -1, 'place-cat' => 'sluzhby-pomoshhi-prizyvnikam', 'orderby' => array( 'title' => 'ASC' ) ) ); ?>
							<?php while ( $query->have_posts() ) { $query->the_post(); ?>
							<option value="<?php the_ID(); ?>"><?php the_title(); ?></option>
							<?php } ?>
						</select>
						<small class="in-org"></small>
					</div>
					<div class="form-group col-xs-12">
						<label for="comment">Ваш отзыв об этой организации</label>
						<textarea class="form-control" name="comment" rows="3" minlength="10" required></textarea>
						<small>Минимум 10 слов.</small>
						<small class="in-comment"></small>
					</div>
					<div class="form-group col-xs-12">
						<label for="rating">Ваша оценка</label>
						<div class="starrating">
							<fieldset class="rating">
								<input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
								<input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
								<input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
								<input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
								<input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
							</fieldset>
							<small class="in-rating"></small>
						</div>
					</div>
					<?php wp_nonce_field( 'new_org_review_nonce' ); ?>
					<button type="submit" class="btn btn-default">Оставить отзыв</button>
				</form>
				<?php
			}elseif(in_array( 'registered', (array) $current_user->roles )){
				echo "Чтобы оставить отзыв необходимо зарегистрироваться на сайте и подтвердить номер телефона.<br>";
				?>
					<?php include ( get_template_directory().'/inc/please_confirm_number.php' ); ?>
				<?php
			}
		}else {
			echo "Чтобы оставить отзыв необходимо зарегистрироваться на сайте и подтвердить номер телефона.<br>";
?>
			<div style="border: 2px solid rgba(0,0,0,0.1); border-radius: 3px; margin-bottom: 20px;">
				<div class="ap-content ap-login" style="margin-top:0px;">
					<div class="ap-cell">
						<div class="ap-cell-inner">
							<div style="max-width: 250px; margin: 0 auto; text-align: center;">
								<?php wp_login_form(); ?>
							</div>
							<div class="ap-login-buttons">
								<a href="<?php echo esc_url( wp_registration_url() ); ?>" class="ap-btn">Зарегистрироваться</a>
								<span class="ap-login-sep">или</span>
								<a href="<?php echo wp_lostpassword_url(); ?>" class="ap-btn">Восстановить пароль</a>
							</div>
						</div>
					</div>
				</div>
			</div>
<?php
		}
	?>

	</div>
<?php get_footer(); ?>
