<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php
					$term = wp_get_post_terms(get_the_ID(), 'wiki-cat', array("fields" => "all"));
					if( $term['0']->parent > 0 ) {
					$term = get_term( $term['0']->parent, 'wiki-cat' );
						$cat = $term->name;
						$cat_slug = $term->slug;
					}else{
						$cat = $term[0]->name;
						$cat_slug = $term[0]->slug;
					}

					$all_posts = new WP_Query(array(
						'post_type' => 'wiki',
						'wiki-cat' => $cat_slug,
						'orderby' => 'name',
						'order' => 'DESC',
						'posts_per_page' => -1
					));
					foreach($all_posts->posts as $key => $value) {
						if($value->ID == $post->ID){
							$nextID = $all_posts->posts[$key + 1]->ID;
							$prevID = $all_posts->posts[$key - 1]->ID;
							break;
						}
					}
					if ( false === get_permalink( $nextID ) ) { $nextID = 0; }
					if ( false === get_permalink( $prevID ) ) { $prevID = 0; }

				?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
						<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemtype="http://schema.org/Thing" itemprop="item" href="/"><i class="fa fa-home" aria-hidden="true"></i><span itemprop="name" class="sr-only">Главная</span></a>
							<meta itemprop="position" content="1" />
						</li>
						<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemtype="http://schema.org/Thing" itemprop="item" href="/wiki/"><span itemprop="name">wiki</span></a>
							<meta itemprop="position" content="2" />
						</li>
						<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
							<a itemtype="http://schema.org/Thing" itemprop="item" href="/wiki/<?php echo $cat_slug; ?>/"><span itemprop="name"><?php echo $cat; ?></span></a>
							<meta itemprop="position" content="3" />
						</li>
						<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active hidden">
							<a itemtype="http://schema.org/Thing" itemprop="item" href="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><span itemprop="name"><?php the_title();?></span></a>
							<meta itemprop="position" content="4" />
						</li>
					</ol>
					<h1><?php the_title();?></h1>
				</div>
				<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
				<script src="//yastatic.net/share2/share.js" async="async"></script>
				<nav>
					<ul class="pager">
						<?php if( $prevID > 0 ): ?>
							<li class="previous"><a href="<?php echo get_permalink($prevID); ?>"><span aria-hidden="true">&larr;</span> Предыдущая</a></li>
						<?php endif; ?>
							<li class="hidden-xs hidden-sm ya-share2-pager-li"><div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,telegram" data-counter=""></div></li>
						<?php if( $nextID > 0 ): ?>
							<li class="next"><a href="<?php echo get_permalink($nextID); ?>">Следующая <span aria-hidden="true">&rarr;</span></a></li>
						<?php endif; ?>
							<li class="hidden-md hidden-lg"><div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,telegram" data-counter=""></div></li>
					</ul>
				</nav>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-full-text">
					<?php the_content(); ?>
				</div>
				<nav>
					<ul class="pager">
						<?php if( $prevID > 0 ): ?>
							<li class="previous"><a href="<?php echo get_permalink($prevID); ?>"><span aria-hidden="true">&larr;</span> Предыдущая</a></li>
						<?php endif; ?>
						<?php if( $nextID > 0 ): ?>
							<li class="next"><a href="<?php echo get_permalink($nextID); ?>">Следующая <span aria-hidden="true">&rarr;</span></a></li>
						<?php endif; ?>
					</ul>
				</nav>
			<?php endwhile; // end of the loop. ?>
			</section>
		</div>
	</div>
<?php get_footer(); ?>
