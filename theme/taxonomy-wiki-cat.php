<?php get_header(); ?>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-full-text">
						<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/"><i class="fa fa-home" aria-hidden="true"></i><span itemprop="name" class="sr-only">Главная</span></a>
								<meta itemprop="position" content="1" />
							</li>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/wiki/"><span itemprop="name">wiki</span></a>
								<meta itemprop="position" content="2" />
							</li>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active hidden">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><span itemprop="name"><?php single_term_title(); ?></span></a>
								<meta itemprop="position" content="3" />
							</li>
						</ol>
						<h1><?php single_term_title(); ?></h1>
						<?php echo term_description() ?>
						<?php if ( have_posts() ) { ?>
							<?php $prev_char = null; ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php
									$term_list = wp_get_post_terms(get_the_ID(), 'wiki-cat', array("fields" => "all"));
									$char = $term_list[0]->name;
									if( $prev_char != $char ){
										$prev_char = $char;
										if($term_list[0]->parent != 0) {
											echo "<h3>". $char ."</h3>";
											echo "<small>".term_description( $term_list[0]->term_id, 'wiki-cat')."</small>";
										}else{
											echo "<br>";
										}
									}
									
									if(empty(get_the_content())){
										echo get_the_title(). "<br>";
									}else{
										echo '<a href="' .get_the_permalink(). '" title="' .esc_attr( strip_tags( get_the_title() ) ). '">'. get_the_title(). '</a><br>';
									}
								?>
								
							<?php endwhile; ?>
						<?php }elseif(empty(term_description())){ ?>
							<p>Sorry, no posts matched your criteria.</p>
						<?php } ?>
					</div>
				</div>
			</div>
<?php get_footer(); ?>
