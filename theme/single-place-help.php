<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php if(has_post_thumbnail( get_the_ID() )) { ?>
					<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>" class="img-responsive">
				<?php }else{ ?>
					<img src="https://via.placeholder.com/1200x550.png?text=<?php the_title_attribute() ?>" class="img-responsive">
				<?php } ?>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php the_content(); ?>
						<table class="table table-bordered">
							<tr>
								<td>Рейтинг</td><td class="note">
									<div class="rating">
										<?php 
											if(get_org_rewiews_count(get_the_ID())){
												$i = 0;
												for ($j = 1; $j <= (int)round(get_field('rating', get_the_ID())); $j++ ) {
													echo '<i class="fa fa-star fa-2x" aria-hidden="true"></i>';
													$i++;
												}
												for($i; $i <5; $i++){
													echo '<i class="fa fa-star-o fa-2x" aria-hidden="true"></i>';
												}
											}else{
												echo "Отзывов пока нет";
											}
										?>
									</div>
								</td>
							</tr>
							<tr><td>Адрес</td><td class="note"><a href="#" class="show-button" data-show="<?php the_ID()?>-adr" data-stat="adr_<?php the_title_attribute() ?>">Показать</a><div class="<?php the_ID(); ?>-adr collapse"><?php the_field('Address', get_the_ID()); ?></div></td></tr>
							<tr><td>Контакты</td><td class="note"><a href="#" class="show-button" data-show="<?php the_ID()?>-cnt" data-stat="cnt_<?php the_title_attribute() ?>">Показать</a><div class="<?php the_ID(); ?>-cnt collapse"><?php the_field('contacts', get_the_ID()); ?></div></td></tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<ul class="nav nav-tabs" role="tablist" id="actions">
							<li role="presentation" class="active"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Обсуждение</a></li>
							<li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Отзывы</a></li>
						</ul>

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="comments">
								<?php comments_template(); ?>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="reviews">
								<?php $result = $wpdb->get_results("SELECT * FROM povestka_wp_company_rating WHERE org_id ='".get_the_ID()."'"); ?>
								<?php if(count($result) >0 ){ ?>
									<?php foreach ( $result as $row ) { ?>
										<?php $user_info = get_userdata($row->user_id); ?>
										<article id="div-comment-6818" class="comment-body media">
											<div class="pull-left">
												<img alt="" src="<?php echo get_avatar_url($row->user_id); ?>" class="avatar avatar-50 photo" height="50" width="50">
											</div>
											<div class="media-body">
												<div class="media-body-wrap panel panel-default">
													<div class="panel-heading">
														<h5 class="media-heading"><b><?php echo $user_info->display_name; ?></b> оставил(а) отзыв <time datetime="<?php echo mysql2date( 'c', $row->date ); ?>"><?php echo mysql2date( 'd.m.Y', $row->date ); ?> в <?php echo mysql2date( 'H:i', $row->date ); ?></time></h5>
													</div>
													<div class="comment-content panel-body">
														<?php echo $row->comment; ?>
														<br>
														<div class="rating">
															<?php 
																$i = 0;
																for ($j = 1; $j <= $row->rating; $j++ ) {
																	echo '<i class="fa fa-star fa-2x" aria-hidden="true"></i>';
																	$i++;
																}
																for($i; $i <5; $i++){
																	echo '<i class="fa fa-star-o fa-2x" aria-hidden="true"></i>';
																}
															?>
														</div>
													</div>
												</div>
											</div><!-- .media-body -->
										</article>
									<?php }?>
								<?php }else{ ?>
									<a href="/rating/#new_org_review">Оставьте первый отзыв</a>
								<?php } ?>
							</div>
						</div>
					
					</div>
				</div>
			<?php endwhile; ?>
			</section>
		</div>
	</div>
<?php get_footer(); ?>