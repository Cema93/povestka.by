<?php get_header(); ?>
		<div class="container">
			<div class="row">
				<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<?php if(is_tax()) { ?>
						<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/"><i class="fa fa-home" aria-hidden="true"></i><span itemprop="name" class="sr-only">Главная</span></a>
								<meta itemprop="position" content="1" />
							</li>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/questions/"><span itemprop="name">Все вопросы</span></a>
								<meta itemprop="position" content="2" />
							</li>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active hidden">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><span itemprop="name"><?php the_title();?></span></a>
								<meta itemprop="position" content="3" />
							</li>
						</ol>
					<?php }elseif(is_single()) { ?>
						<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/"><i class="fa fa-home" aria-hidden="true"></i><span itemprop="name" class="sr-only">Главная</span></a>
								<meta itemprop="position" content="1" />
							</li>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/questions/"><span itemprop="name">Все вопросы</span></a>
								<meta itemprop="position" content="2" />
							</li>
							<?php $cur_term = get_the_terms( $post->ID, 'question_category' ); ?>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="/questions/categories/<?php echo $cur_term[0]->slug; ?>/"><span itemprop="name"><?php echo $cur_term[0]->name; ?></span></a>
								<meta itemprop="position" content="3" />
							</li>
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active hidden">
								<a itemtype="http://schema.org/Thing" itemprop="item" href="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>"><span itemprop="name"><?php the_title();?></span></a>
								<meta itemprop="position" content="4" />
							</li>
						</ol>
					<?php } ?>
					
					<?php if(!is_tax()) { ?>
						<h1><?php the_title(); ?></h1>
					<?php } ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>
				</section>
			</div>
		</div>
	</section>
<?php get_footer(); ?>
