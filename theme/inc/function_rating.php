<?php

function get_org_rewiews_count($org_id){
	global $wpdb;
	$count = $wpdb->get_var("SELECT COUNT(*) FROM povestka_wp_company_rating WHERE org_id ='".$org_id."'");
	return $count;
}

function recount_org_reviews($org_id){
	global $wpdb;
	
	$result = $wpdb->get_col("SELECT rating FROM povestka_wp_company_rating WHERE org_id ='".$org_id."'");
	if($result == null) { 
		$average=0; 
	} else {
		$average = array_sum($result) / count($result);
		$average = round($average, 2);
	}
	if(update_field( 'rating', $average, (int)$org_id )) {return true;}else{return false;}
}
add_action('admin_menu', function(){
	$hook_suffix = add_menu_page( 'Панель', 'Рейтинг организаций', 'manage_options', 'org-rating', 'org_rating_page', '', 4 );
	add_action( "load-$hook_suffix", 'org_rating_page_save_action' );
} );

function org_rating_page_save_action() {
	global $action_message, $wpdb;
	$requestType = $_SERVER['REQUEST_METHOD'];
	$vk_feed_action_message = '';
	if($requestType == 'POST'){
		if( isset($_POST['id']) AND ctype_digit($_POST['id'])) {
			$id = (int)$_POST['id'];
			if (isset($_POST['delete'])) {
				$org_id = $wpdb->get_var("SELECT org_id FROM povestka_wp_company_rating where id=".$id);
				$result = $wpdb->delete( 'povestka_wp_company_rating', array( 'id' => $id ), array( '%d' ) );
				if($result){
					recount_org_reviews($org_id);
					$action_message = '<div id="message" class="updated">Отзыв '. $id .' удалён</div>';
				}
			}
		}else{
			wp_die('Есть пост, но нет id');
		}
	}
}

function org_rating_page(){
	global $wpdb, $action_message;
	?>
	<div class="wrap">
		<h2>Отзывы</h2>
		<?php echo $action_message; ?>
		<?php $result = $wpdb->get_results("SELECT * FROM povestka_wp_company_rating"); ?>
		<?php if(count($result) >0 ){ ?>
			<?php $i=0; ?>
			<table class="widefat">
				<thead>
					<tr>
						<th class="row-title">Автор</th>
						<th>Организация</th>
						<th>Оценка</th>
						<th>Комментарий</th>
						<th>Дата</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ( $result as $row ) { ?>
						<?php $user_info = get_userdata($row->user_id); ?>
						<tr<?php if( $i++%2 == 0 ){ echo ' class="alternate"'; }?>>
							<td class="row-title"><?php echo $user_info->display_name; ?></td>
							<td><?php echo get_the_title($row->org_id); ?></td>
							<td><?php echo $row->rating; ?></td>
							<td><?php echo $row->comment; ?></td>
							<td><?php echo $row->date; ?></td>
							<td><form method="POST"><input type="hidden" name="id" value="<?php echo $row->id; ?>"><?php submit_button('Удалить', 'delete', 'delete', false ); ?></form></td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		<?php }else{?>
			<p>Отзывов ещё нет</p>
		<?php }?>
	</div>
	<?php
}
?>