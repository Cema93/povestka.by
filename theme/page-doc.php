<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<section class="content-block">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ul class="nav nav-tabs row" role="tablist" id="tabs">
						<li role="presentation" class="active col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="section-wrapper">
								<a href="#documents" role="tab" data-toggle="tab">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sections/1.png" alt="" />
									<p>Образцы заявлений и жалоб</p>
								</a>
								<div class="section-border-1"></div>
								<div class="section-border-2"></div>
								<div class="section-border-3"></div>
								<div class="section-border-5"></div>
							</div>
						</li>
						<li role="presentation" class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="section-wrapper">
								<a href="#medical" role="tab" data-toggle="tab">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sections/2.png" alt="" />
									<p>Медицинские противопоказания для службы в армии</p>
								</a>
								<div class="section-border-1"></div>
								<div class="section-border-2"></div>
								<div class="section-border-3"></div>
								<div class="section-border-5"></div>
							</div>
						</li>
						<li role="presentation" class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="section-wrapper">
								<a href="#law" role="tab" data-toggle="tab">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sections/3.png" alt="" />
									<p>Нормативно-правовые акты</p>
								</a>
								<div class="section-border-1"></div>
								<div class="section-border-2"></div>
								<div class="section-border-3"></div>
								<div class="section-border-5"></div>
							</div>
						</li>
						<li role="presentation" class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
							<div class="section-wrapper">
								<a href="#gos" role="tab" data-toggle="tab">
									<img src="<?php echo get_template_directory_uri(); ?>/img/sections/4.png" alt="" />
									<p>Разъяснения от государственных органов и комментарии к законодательству</p>
								</a>
								<div class="section-border-1"></div>
								<div class="section-border-2"></div>
								<div class="section-border-3"></div>
								<div class="section-border-5"></div>
							</div>
						</li>
					</ul>
					<?php the_content(); ?>
				</div>	
			</div>
		</div>
	</section>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
