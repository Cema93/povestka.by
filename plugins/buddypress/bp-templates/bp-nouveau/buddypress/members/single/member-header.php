<?php
/**
 * BuddyPress - Users Header
 *
 * @since 3.0.0
 * @version 3.0.0
 */
	global $current_user;
	get_currentuserinfo();
?>

<div id="item-header-avatar">
	<a href="<?php bp_displayed_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?>

	</a>
</div><!-- #item-header-avatar -->

<div id="item-header-content">

		<h2 class="user-nicename"><?php echo $current_user->display_name; ?> <small><?php bp_nouveau_member_hook( 'before', 'header_meta' ); ?></small></h2>

	<?php if ( bp_nouveau_member_has_meta() AND current_user_can('manage_options') ) : ?>

		<div class="item-meta">

			<?php bp_nouveau_member_meta(); ?>

		</div><!-- #item-meta -->
	<?php endif; ?>

	<?php bp_nouveau_member_header_buttons( array( 'container_classes' => array( 'member-header-actions' ) ) ); ?>
</div><!-- #item-header-content -->
